module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = req.query.q
    let answer = ''

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    if (/what is your name/.test(question)) {
      answer = 'Robert'
    } else if (/which of the following numbers is the largest/.test(question)) {
      let numbers = question.match(/\d+/g).map(Number)
      answer = Math.max(...numbers)
    } else if (/what is (.*) plus (.*) plus (.*)/.test(question)) {
      var numbers4 = question.match(/\d+/g).map(Number)
      answer = numbers4.reduce((a, b) => a + b, 0)
    } else if (/what is (.*) plus (.*)/.test(question)) {
      var numbers2 = question.match(/\d+/g).map(Number)
      answer = numbers2.reduce((a, b) => a + b, 0)
    } else if (/what is (.*) minus (.*)/.test(question)) {
      var numbers3 = question.match(/\d+/g).map(Number)
      answer = numbers3[0] - numbers3[1]
    } else if (/what is (.*) to the power of (.*)/.test(question)) {
      var numbers5 = question.match(/\d+/g).map(Number)
      answer = Math.pow(numbers5[0], numbers5[1])
    } else {
      answer = question || 'Hello Hackathon'
    }

    // } else if (/what is/.test(question)) {
    //   var numbers2 = question.match(/\d+/g).map(Number)
    //   const arrSum = arr => arr.reduce((a, b) => a + b, 0)
    //   answer = arrSum(numbers2)

    // Answer the question
    res.status(200).send(String(answer))
  }
}
