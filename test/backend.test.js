const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  it('should say my name', done => {
    agent
      .get('/api')
      .query({ q: 'ecc88390: what is your name' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Robert')
        done()
      })
  })

  it('should say 412', done => {
    agent
      .get('/api')
      .query({ q: 'which of the following numbers is the largest: 38, 412, 47, 339' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('412')
        done()
      })
  })

  it('should say 26', done => {
    agent
      .get('/api')
      .query({ q: 'what is 9 plus 17' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('26')
        done()
      })
  })

  it('should say 31', done => {
    agent
      .get('/api')
      .query({ q: 'what is 17 plus 2 plus 12' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('31')
        done()
      })
  })

  it('should say 4', done => {
    agent
      .get('/api')
      .query({ q: 'what is 18 minus 14' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('4')
        done()
      })
  })

  it('should say 4', done => {
    agent
      .get('/api')
      .query({ q: 'what is 4 to the power of 1' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('4')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
